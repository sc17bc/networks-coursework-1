import java.net.*;
import java.util.*;
import java.io.*;

public class Coursework1 {
    private static int hostInfo (InetAddress address){
        System.out.println(address.getHostName()); //Prints the hostname
        System.out.println(address.getHostAddress()); //Prints the IP

        try {
            if (address.isReachable(5000)){ //Says whether or not the host is reachable with a 5s timeout
                System.out.println("The host is reachable");
            } else {
                System.out.println("The host is not reachable");
            }
        } catch (IOException e){
            System.out.println("A network error occured");
        }

        int addressType = address.getAddress().length; //Used to differentiate between IPv4 and IPv6

        if (addressType == 4) {
            System.out.println("IPv4");
        } else if (addressType == 16) {
            System.out.println("IPv6");
        }

        System.out.println("----------------------");
        return addressType;
    }

    public static void main(String[] args) throws UnknownHostException, IOException {
        int i = 0, j = 0;
        try { //Stops the program trying to find addresses when none exist
            if (args.length < 1){
                throw new Exception("No arguments were provided");
            }
        } catch (Exception e){
            System.out.println(e.toString());
            return;
        }

        ArrayList<InetAddress> addressList = new ArrayList<InetAddress>(); //Initialising the list of InetAddress objects
        InetAddress address = null;

        for (i = 0; i < args.length; i++){ //Assigning all the entered addresses to InetAddress objects
            address = address.getByName(args[i]);
            addressList.add(address);
        }

        ArrayList<InetAddress> IPv4List = new ArrayList<InetAddress>();
        int IPv4Count = 0;

        for (i = 0; i < args.length; i++){ //Making a list of all the IPv4 addresses
            if (hostInfo(addressList.get(i)) == 4) {
                IPv4List.add(addressList.get(i));
                IPv4Count++;
            }
        }

        if (IPv4Count > 0) { //Printing a shared part of the IP if there is one
            String[] firstIP = IPv4List.get(0).getHostAddress().split("\\.");
            boolean[] matches = {true, true, true, true};
            for (i = 1; i < IPv4Count; i++) {
                matches[0] = true;
                matches[1] = true;
                matches[2] = true;
                matches[3] = true;
                for (j = 0; j <= 3 && matches[0] == true; j++) {
                    if (!IPv4List.get(i).getHostAddress().split("\\.")[j].equals(firstIP[j])) {
                        matches[j] = false;
                    }
                }
            }

            boolean equal = true;
            String sharedIP = "";
            for (i = 0; i <= 3; i++) {
                if (!matches[i]) {
                    equal = false;
                }
                if (equal) {
                    sharedIP = sharedIP + firstIP[i];
                } else {
                    sharedIP = sharedIP + "*";
                }
                if (i != 3) {
                    sharedIP = sharedIP + ".";
                }
            }

            System.out.println(sharedIP);
        }
    }
}
